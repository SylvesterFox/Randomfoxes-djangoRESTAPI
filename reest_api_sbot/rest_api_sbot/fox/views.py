from django.shortcuts import render
from rest_framework import generics
from .serializers import FoxSerializersView
from random import randrange
from .models import Fox
from rest_framework.permissions import IsAdminUser
from random import choice

# Create your views here.


class FoxViewSet(generics.CreateAPIView):
    serializer_class = FoxSerializersView
    permission_classes = (IsAdminUser, )


class FoxViewRandom(generics.ListAPIView):
    serializer_class = FoxSerializersView

    def get_queryset(self):
        pks = Fox.objects.values_list('pk', flat=True).order_by('id')
        random_pks = choice(pks)
        return Fox.objects.all().filter(id=random_pks)



