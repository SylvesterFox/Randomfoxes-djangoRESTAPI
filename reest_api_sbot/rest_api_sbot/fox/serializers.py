from .models import Fox
from rest_framework import serializers


class FoxSerializersView(serializers.ModelSerializer):
    class Meta:
        model = Fox
        fields = '__all__'