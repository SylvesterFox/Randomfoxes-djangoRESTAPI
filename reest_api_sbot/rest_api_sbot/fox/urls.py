from django.contrib import admin
from django.urls import path
from .views import *

app_name = 'fox'
urlpatterns = [
    path('fox/create', FoxViewSet.as_view()),
    path('fox/random', FoxViewRandom.as_view())
]
