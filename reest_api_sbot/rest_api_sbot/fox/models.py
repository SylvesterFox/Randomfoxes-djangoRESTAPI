from django.db import models
from django.contrib.auth import get_user_model
user = get_user_model()

# Create your models here.
class Fox(models.Model):
    BREED_TYPE = (
        ("Vulpes vulpes", "Vulpes vulpes"),
        ("Vulpes velox", "Vulpes velox"),
        ("Vulpes cana", "Vulpes cana"),
        ("Vulpes pallida", "Vulpes pallida"),
        ("Vulpes bengalensis", "Vulpes bengalensis"),
        ("Vulpes corsac", "Vulpes corsac"),
        ("Vulpes rueppelli", "Vulpes rueppelli"),
        ("Vulpes ferrilata", "Vulpes ferrilata"),
        ("Vulpes zerda", "Vulpes zerda"),
        ("Vulpes chama", "Vulpes chama"),
        ("Alopex lagopus", "Alopex lagopus"),
        ("Urocyon cinereoargenteus", "Urocyon cinereoargenteus"),
        ("Urocyon littoralis", "Urocyon littoralis"),
        ("Dusicyon australis", "Dusicyon australis"),
        ("Cerdocyon thous", "Cerdocyon thous"),
        ("Atelocynus microtis", "Atelocynus microtis"),
        ("Lycalopex culpaeus", "Lycalopex culpaeus"),
        ("Lycalopex griseus", "Lycalopex griseus"),
        ("Lycalopex fulvipes", "Lycalopex fulvipes"),
        ("Lycalopex gymnocercus", "Lycalopex gymnocercus"),
        ("Lycalopex vetulus", "Lycalopex vetulus"),
        ("Lycalopex sechurae", "Lycalopex sechurae"),
        ("Otocyon", "Otocyon")
    )
    image = models.ImageField(default="default.png")
    breed_type = models.TextField(verbose_name="Вид:", choices=BREED_TYPE)
    user = models.ForeignKey(user, verbose_name="User", on_delete=models.CASCADE)
